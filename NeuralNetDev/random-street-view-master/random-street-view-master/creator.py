from __future__ import print_function
from os import listdir
from os.path import isfile, join
import os
import csv
import re

mypath = os.path.dirname(os.path.realpath(__file__))
onlyfiles = [f for f in listdir(mypath + "/Images") if isfile(join(mypath + "/Images", f))]
regex = re.compile('[^a-zA-Z]')
os.remove("labels.csv")

with open('labels.csv', 'w+') as csvfile:
    for f in onlyfiles:
        print(f)
        writer = csv.writer(csvfile, lineterminator = '\n')
        test = regex.sub('', f)
        test = test.replace("jpg", "")
        writer.writerow([f, test])
