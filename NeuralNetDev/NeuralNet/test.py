""" 
Author: William Leingang
Project: NeuralNet
Disclaimer: This project is somewhat loosely based off of code found here:
https://github.com/aymericdamien/TensorFlow-Examples/ However, it really
didn't work for what I needed so only a few pieces of my original follow 
through of his tutorial remain, such as some elements of model building and
the display step printing.
"""


from __future__ import print_function
import os, os.path
import gzip
import time
import tensorflow as tf
import numpy as np
import math
from tensorflow.contrib import rnn
from tensorflow.examples.tutorials.mnist import input_data
import glob

# Training Parameters
learning_rate = 0.001
batch_size = 40
display_step = 20

# Network Parameters
num_input = 34 # Image height
timesteps = 35 # Image width
num_hidden = 128 # hidden layer num of features
num_classes = 4 # Number of classes
percent_test = .1
training_steps = 1000
dataset_dir = "C:/Users/lego_/source/repos/NeuralNetDev/random-street-view-master/random-street-view-master/test/*/*.jpg"

print("Building model.")

# Define weights
weights = {
    'out': tf.Variable(tf.random_normal([num_hidden, num_classes]))
}
biases = {
    'out': tf.Variable(tf.random_normal([num_classes]))
}

# Images
x = tf.placeholder("float", [None, timesteps, num_input])
# Labels
y = tf.placeholder("float", [None, num_classes])

input = tf.unstack(x, timesteps, 1)

lstm_layer = rnn.BasicLSTMCell(num_hidden, forget_bias=1)

outputs, _ = rnn.static_rnn(lstm_layer, input, dtype="float32")

prediction = tf.matmul(outputs[-1], weights['out']) + biases['out']

loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels = y))

#Proved to have better results than previous optimizer used.
opt = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

accsTrain = []

correct_prediction=tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
accuracy=tf.reduce_mean(tf.cast(correct_prediction,tf.float32))

init = tf.global_variables_initializer()


print("Reading in dataset.")
filename_queue = tf.train.string_input_producer(
    tf.train.match_filenames_once(dataset_dir))
image_reader = tf.WholeFileReader()
key, image_file = image_reader.read(filename_queue)
S = tf.string_split([key],'\\')
length = tf.cast(S.dense_shape[1],tf.int32)
label = S.values[length-tf.constant(2,dtype=tf.int32)]

image = tf.image.decode_image(image_file, channels=1)
label = tf.string_to_number(label,out_type=tf.int32)

#Ugly way to get the length of the dataset.
dataset_length = 0
for file in glob.glob(dataset_dir):
    dataset_length = dataset_length + 1

print(dataset_length)
test_length = math.floor(dataset_length*(percent_test))

print("Formatting dataset")
with tf.Session() as sess:
    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())
    tf.global_variables_initializer().run()

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    temporaryImages = np.zeros([timesteps, num_input, 1])
    temporaryLabels = np.zeros([num_classes])
    
    for i in range(dataset_length):
        # Get an image tensor
        key_val,label_val,image_tensor = sess.run([key,label,image])
        temporaryImages = np.append(temporaryImages, image_tensor)

        # This part is hard coded for 4 classes. Later I will be exploring other methods of performing this task that allow for more flexability.
        if(label_val == 1):
            temporaryLabels = np.append(temporaryLabels, [1, 0, 0, 0])
        elif(label_val == 2):
            temporaryLabels = np.append(temporaryLabels, [0, 1, 0, 0])
        elif(label_val == 3):
            temporaryLabels = np.append(temporaryLabels, [0, 0, 1, 0])
        elif(label_val == 4):
            temporaryLabels = np.append(temporaryLabels, [0, 0, 0, 1])

    #Removing the initialized values.
    temporaryLabels = temporaryLabels[num_classes:]
    temporaryImages = temporaryImages[(timesteps*num_input):]

   # Finish off the filename queue coordinator.
    coord.request_stop()
    coord.join(threads)
    
    # Commented Out Batching
    ###############training_steps = dataset_length

    temporaryImageTest, temporaryImageTrain  = np.split(temporaryImages, [math.floor(timesteps*num_input*dataset_length*(percent_test))])
    temporaryLabelTest, temporaryLabelTrain  = np.split(temporaryLabels, [math.floor(num_classes*dataset_length*(percent_test))])

    iter = 1
    batches = 0
    
    batch_x=temporaryImageTrain.reshape([math.floor(dataset_length*(1-percent_test)),timesteps,num_input])
    batch_y=temporaryLabelTrain.reshape([math.floor(dataset_length*(1-percent_test)),num_classes])
    

    trainTime = time.time()
    print("Beginning training.")
    while iter<training_steps:
        # Run the neural network.
        sess.run(opt, feed_dict={x: batch_x, y: batch_y})

        # Displays in progress data to show improvements and that it is really doing something and isn't stuck.
        if iter % display_step == 0:
            acc=sess.run(accuracy,feed_dict={x:batch_x,y:batch_y})
            los=sess.run(loss,feed_dict={x:batch_x,y:batch_y})
            print("For iter ",iter)
            print("Accuracy ",acc)
            print("Loss ",los)
            print("__________________")
            accsTrain.append(acc)

        iter=iter+1
        batches=batches+1
        
    trainTime = time.time() - trainTime



    print("Beginning testing.")
    testTime = time.time()

    test_data=temporaryImageTest.reshape([test_length,timesteps,num_input])
    test_label=temporaryLabelTest.reshape([test_length,num_classes])

    test_accuracy = sess.run(accuracy, feed_dict={x: test_data, y: test_label})

    testTime = time.time() - testTime
    
    predictions = tf.argmax(prediction ,1)

    # Retrieving values for confusion matrix.
    predictions_2 = predictions.eval(feed_dict={x: test_data, y: test_label}, session=sess)
    
    test_label_2 = list()

    i = 0
    while i<test_length:
        test_label_2.append(test_label[i].argmax())
        i = i+1

    table = []

    i = 0
    while i<num_classes:
        new = []
        i2 = 0
        while i2<num_classes:
            new.append(0)
            i2 = i2+1
        table.append(new)
        i = i+1

    i = 0
    while i<test_length:
        table[test_label_2[i]][predictions_2[i]] = table[test_label_2[i]][predictions_2[i]]+1;
        i = i+1

    # Prints the confusion matrix.
    i = 0
    print("Confusion Matrix")
    while i<num_classes:
        print(table[i])
        i = i+1
  
    print("Training Time in Seconds: ", trainTime)
    print("Testing Time in Seconds: ", testTime)
    print("Testing Accuracy: ", test_accuracy)

    np.set_printoptions(suppress=True)

    # Saves data to a record file.
    f = open('record.txt', 'w+')
    f.write("Training Time in Seconds: " + str(trainTime) + "\n")
    f.write("Testing Time in Seconds: " + str(testTime) + "\n")
    f.write("Testing Accuracy: " + str(test_accuracy) + "\n")
    f.write("Confusion Matrix \n")
    np.savetxt(f,table, fmt='%i')
    f.close()

    # Saves the trained model for later use.
    saver = tf.train.Saver()
    saver.save(sess, "C:/Users/lego_/source/repos/NeuralNetDev/trained_model")