from __future__ import print_function
import os, os.path
import gzip
import time
import tensorflow as tf
import numpy as np
import math
from tensorflow.contrib import rnn
from tensorflow.examples.tutorials.mnist import input_data
import glob


# Training Parameters
learning_rate = 0.001
batch_size = 40
display_step = 20

# Network Parameters
num_input = 34
timesteps = 35
num_hidden = 128 
num_classes = 4 

print("Before Start")

# Define weights
weights = {
    'out': tf.Variable(tf.random_normal([num_hidden, num_classes]))
}
biases = {
    'out': tf.Variable(tf.random_normal([num_classes]))
}

# Images
x = tf.placeholder("float", [None, timesteps, num_input])
# Labels
y = tf.placeholder("float", [None, num_classes])

input = tf.unstack(x, timesteps, 1)

lstm_layer = rnn.BasicLSTMCell(num_hidden, forget_bias=1)


outputs, _ = rnn.static_rnn(lstm_layer, input, dtype="float32")

prediction = tf.matmul(outputs[-1], weights['out']) + biases['out']

loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels = y))

opt = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)


accsTrain = []


correct_prediction=tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
accuracy=tf.reduce_mean(tf.cast(correct_prediction,tf.float32))


init = tf.global_variables_initializer()


print("Start")
filename_queue = tf.train.string_input_producer(
    tf.train.match_filenames_once("C:/Users/lego_/source/repos/NeuralNetDev/random-street-view-master/random-street-view-master/LoadTest/*/*.jpg"))
image_reader = tf.WholeFileReader()
key, image_file = image_reader.read(filename_queue)
S = tf.string_split([key],'\\')
length = tf.cast(S.dense_shape[1],tf.int32)
# adjust constant value corresponding to your paths if you face issues. It should work for above format.
label = S.values[length-tf.constant(2,dtype=tf.int32)]

image = tf.image.decode_image(image_file, channels=1)
label = tf.string_to_number(label,out_type=tf.int32)


dataset_length = 0
for file in glob.glob("C:/Users/lego_/source/repos/NeuralNetDev/random-street-view-master/random-street-view-master/LoadTest/*/*.jpg"):
    dataset_length = dataset_length + 1

print(dataset_length)


with tf.Session() as sess:
    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())
    tf.global_variables_initializer().run()

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    temporaryImages = np.zeros([timesteps, num_input, 1])
    temporaryLabels = np.zeros([num_classes])


    for i in range(dataset_length):
        # Get an image tensor
        key_val,label_val,image_tensor = sess.run([key,label,image])
        temporaryImages = np.append(temporaryImages, image_tensor)

        if(label_val == 1):
            temporaryLabels = np.append(temporaryLabels, [1, 0, 0, 0])
        elif(label_val == 2):
            temporaryLabels = np.append(temporaryLabels, [0, 1, 0, 0])
        elif(label_val == 3):
            temporaryLabels = np.append(temporaryLabels, [0, 0, 1, 0])
        elif(label_val == 4):
            temporaryLabels = np.append(temporaryLabels, [0, 0, 0, 1])

    temporaryLabels = temporaryLabels[num_classes:]
    temporaryImages = temporaryImages[(timesteps*num_input):]


   # Finish off the filename queue coordinator.
    coord.request_stop()
    coord.join(threads)
    
    print("Start")

    ###############training_steps = dataset_length
    training_steps = 1000

    saver = tf.train.Saver()
    saver.restore(sess,tf.train.latest_checkpoint("C:/Users/lego_/source/repos/NeuralNetDev/"))


    testTime = time.time()


    test_data=temporaryImages.reshape([dataset_length,timesteps,num_input])
    test_label=temporaryLabels.reshape([dataset_length,num_classes])

    test_accuracy = sess.run(accuracy, feed_dict={x: test_data, y: test_label})

    testTime = time.time() - testTime


    predictions = tf.argmax(prediction ,1)



    predictions_2 = predictions.eval(feed_dict={x: test_data, y: test_label}, session=sess)
    
    i = 0

    test_label_2 = list()


        # !TEMP
    while i<dataset_length:
        test_label_2.append(test_label[i].argmax())
        i = i+1


    table = []

    i = 0
    while i<num_classes:
        new = []
        i2 = 0
        while i2<num_classes:
            new.append(0)
            i2 = i2+1
        table.append(new)
        i = i+1


    i = 0
    while i<dataset_length:
        table[test_label_2[i]][predictions_2[i]] = table[test_label_2[i]][predictions_2[i]]+1;
        i = i+1


    i = 0
    print("Confusion Matrix")
    while i<num_classes:
        print(table[i])
        i = i+1
  

    print("Testing Time in Seconds: ", testTime)
    print("Testing Accuracy: ", test_accuracy)






