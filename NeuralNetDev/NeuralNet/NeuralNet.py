""" Recurrent Neural Network.
A Recurrent Neural Network (LSTM) implementation example using TensorFlow library.
This example is using the MNIST database of handwritten digits (http://yann.lecun.com/exdb/mnist/)
Links:
    [Long Short Term Memory](http://deeplearning.cs.cmu.edu/pdfs/Hochreiter97_lstm.pdf)
    [MNIST Dataset](http://yann.lecun.com/exdb/mnist/).
Author: Aymeric Damien
Project: https://github.com/aymericdamien/TensorFlow-Examples/
"""


from __future__ import print_function
import os, os.path
import gzip
import time
import tensorflow as tf
import numpy as np
from tensorflow.contrib import rnn
from tensorflow.examples.tutorials.mnist import input_data
import glob

'''
To classify images using a recurrent neural network, we consider every image
row as a sequence of pixels. Because MNIST image shape is 28*28px, we will then
handle 28 sequences of 28 steps for every sample.
'''

# Training Parameters
learning_rate = 0.001
training_steps = 12
batch_size = 3
display_step = 3

# Network Parameters
num_input = 200 # MNIST data input (img shape: 200*200) 
timesteps = 200 # timesteps
num_hidden = 128 # hidden layer num of features
num_classes = 4 # MNIST total classes (0-9 digits)
test_len = 300



print("Before Start")

# Define weights
weights = {
    'out': tf.Variable(tf.random_normal([num_hidden, num_classes]))
}
biases = {
    'out': tf.Variable(tf.random_normal([num_classes]))
}

# Images
x = tf.placeholder("float", [None, timesteps, num_input])
# Labels
y = tf.placeholder("float", [None, num_classes])

input = tf.unstack(x, timesteps, 1)

lstm_layer = rnn.BasicLSTMCell(num_hidden, forget_bias=1)


"""Not yet fully understood"""
outputs, _ = rnn.static_rnn(lstm_layer, input, dtype="float32")

prediction = tf.matmul(outputs[-1], weights['out']) + biases['out']

loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=prediction, labels = y))

opt = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)


accsTrain = []


correct_prediction=tf.equal(tf.argmax(prediction,1),tf.argmax(y,1))
accuracy=tf.reduce_mean(tf.cast(correct_prediction,tf.float32))


init = tf.global_variables_initializer()




print("Start")
filename_queue = tf.train.string_input_producer(
    tf.train.match_filenames_once("C:/Users/lego_/source/repos/NeuralNet/random-street-view-master/random-street-view-master/Test/*/*.jpg"))
image_reader = tf.WholeFileReader()
key, image_file = image_reader.read(filename_queue)
S = tf.string_split([key],'\\')
length = tf.cast(S.dense_shape[1],tf.int32)
# adjust constant value corresponding to your paths if you face issues. It should work for above format.
label = S.values[length-tf.constant(2,dtype=tf.int32)]

image = tf.image.decode_image(image_file, channels=1)
label = tf.string_to_number(label,out_type=tf.int32)


training_steps = 0
for file in glob.glob("C:/Users/lego_/source/repos/NeuralNet/random-street-view-master/random-street-view-master/Test/*/*.jpg"):
    training_steps = training_steps + 1

print(training_steps)
trainTime = time.time()

# Start a new session to show example output.
with tf.Session() as sess:
    sess.run(tf.local_variables_initializer())
    sess.run(tf.global_variables_initializer())

    # Required to get the filename matching to run.
    tf.initialize_all_variables().run()

    # Coordinate the loading of image files.
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    test1 = np.zeros([timesteps, num_input, 1])
    test2 = np.zeros([num_classes])


    for i in range(training_steps):
        # Get an image tensor and print its value.
        key_val,label_val,image_tensor = sess.run([key,label,image])
        #print(image_tensor.shape)
        #print(key_val)
        #print(label_val)
        test1 = np.append(test1, image_tensor)

        if(label_val == 1):
            test2 = np.append(test2, [1, 0, 0, 0])
        elif(label_val == 2):
            test2 = np.append(test2, [0, 1, 0, 0])
        elif(label_val == 3):
            test2 = np.append(test2, [0, 0, 1, 0])
        elif(label_val == 4):
            test2 = np.append(test2, [0, 0, 0, 1])

    #An ugly way of removing the initialized values.
    test2 = np.delete(test2, 0)
    test2 = np.delete(test2, 0)
    test2 = np.delete(test2, 0)
    test2 = np.delete(test2, 0)

    #An ugly way of removing the initialized values.
    for i in range(0, (timesteps*num_input)):
        test1 = np.delete(test1, 0)



   # Finish off the filename queue coordinator.
    coord.request_stop()
    coord.join(threads)
    
    iter = 1





    print("Start")

    batches = 0
    while iter<training_steps:
        
        ####################################

        #filename_queue = tf.train.string_input_producer(filenames)

        ##step 3: read, decode and resize images
        #reader = tf.WholeFileReader()
        #filename, content = reader.read(filename_queue)
        #image = tf.image.decode_jpeg(content, channels=3)
        #image = tf.cast(image, tf.float32)
        #resized_image = tf.image.resize_images(image, [200, 200])

        ##step 4: Batching
        #batch_x = tf.train.batch([resized_image], batch_size=8)
        #######################

        ###NEW!###        


        batch_x=test1.reshape([training_steps,timesteps,num_input])
        batch_y=test2.reshape([training_steps,num_classes])



  




        sess.run(opt, feed_dict={x: batch_x, y: batch_y})


        if iter % display_step == 0:
            acc=sess.run(accuracy,feed_dict={x:batch_x,y:batch_y})
            los=sess.run(loss,feed_dict={x:batch_x,y:batch_y})
            print("For iter ",iter)
            print("Accuracy ",acc)
            print("Loss ",los)
            print("__________________")
            accsTrain.append(acc)

        iter=iter+1

    trainTime = time.time() - trainTime

    testTime = time.time()

    test_data = batch_x

    test_label = batch_y

    test_accuracy = sess.run(accuracy, feed_dict={x: test_data, y: test_label})

    testTime = time.time() - testTime


    predictions = tf.argmax(prediction ,1)



    predictions_2 = predictions.eval(feed_dict={x: test_data, y: test_label}, session=sess)
    
    i = 0

    test_label_2 = list()

    while i<test_len:
        test_label_2.append(test_label[i].argmax())
        i = i+1


    table = []

    i = 0
    while i<num_classes:
        new = []
        i2 = 0
        while i2<num_classes:
            new.append(0)
            i2 = i2+1
        table.append(new)
        i = i+1


    i = 0
    while i<test_len:
        table[test_label_2[i]][predictions_2[i]] = table[test_label_2[i]][predictions_2[i]]+1;
        i = i+1


    i = 0
    print("Confusion Matrix")
    while i<num_classes:
        print(table[i])
        i = i+1
  



    print("Training Time in Seconds: ", trainTime)
    print("Testing Time in Seconds: ", testTime)
    print("Testing Accuracy: ", test_accuracy)





    np.set_printoptions(suppress=True)

    f = open('record.txt', 'w+')

    f.write("Training Time in Seconds: " + str(trainTime) + "\n")
    f.write("Testing Time in Seconds: " + str(testTime) + "\n")
    f.write("Testing Accuracy: " + str(test_accuracy) + "\n")
    f.write("Confusion Matrix \n")
    np.savetxt(f,table, fmt='%i')
    f.close()