# Country Identification using Machine Learning

Welcome to the CIML repository. CIML is an attempt to identify countries using images from Google Street View. 


## Our Neural Network

For this project we are using a Long Short Term Memory Neural Network which is implemented through the Tensorflow library.
This neural network can easily be applied to any dataset of jpeg images. However, at the moment it is hardcoded for four
class classification. This should only be temporary and will be updated in the future.


## Our Dataset

Our current dataset is only 1600 200px x 200px images of four different countries. We gathered this images using a script found here: https://github.com/hugovk/random-street-view.
A modified version of this script is in this repository, but I highly recommend you go and get the original if you want to get random street view images. The one
here is very similar, and all credit goes to the original coder. 



## Learn More

If you have any questions about this project, or suggestions on changes, email me at wleingang15@mail.wou.edu. I'll get sure to get back to you, one way or another. 


# Who Am I?

#### William Leingang

> I'm a software engineer with experience in quite a few languages. While I'm primarily programmer, my real passion is writing. I try to merge the two as often as possible, whether that be in 
> coding projects such as Timeline, or trying to get story prompts to be generated through markov chains. 
>  
> Through an REU (Research Experience for Undergraduates) I became experienced in machine learning techniques. 
> 
> I plan to graduate in Spring of 2018 with a Bachelors in Computer Science from Western Oregon University. 


# Contributing

CIML is an open source project, so anyone can contribute. If you want to help us by adding to our codebase or fixing bugs at that time, please feel free to. Once again, if you have any questions, send them wleingang15@mail.wou.edu.


## Getting Started

If this is your first time coding using a forked repository and pull requests to contribute to a project, I highly recommend using 
[this](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow) tutorial. It walks you through step by step instructions on everything from initially forking the repository to
making your first pull request. 